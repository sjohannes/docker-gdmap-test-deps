FROM alpine

RUN apk add --no-cache \
	gcc \
	desktop-file-utils \
	gettext \
	gtk+3.0-dev \
	libc-dev \
	libxml2-dev \
	meson
